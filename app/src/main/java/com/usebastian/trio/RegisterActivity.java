package com.usebastian.trio;

import androidx.appcompat.app.AppCompatActivity;
import es.dmoral.toasty.Toasty;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class RegisterActivity extends AppCompatActivity
{

	//Declaration EditTexts
	EditText editTextName, editTextEmail, editTextPassword;

	//Declaration Button
	Button buttonRegister;

	//Database
	SQLiteHelper sqLiteHelper;

	@Override
	protected void onCreate( Bundle savedInstanceState )
	{
		super.onCreate( savedInstanceState );
		setContentView( R.layout.activity_register );

		sqLiteHelper = new SQLiteHelper(this);
		initViews();


		buttonRegister.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if (validate()) {
					String UserName = editTextName.getText().toString();
					String Email = editTextEmail.getText().toString();
					String Password = editTextPassword.getText().toString();

					//Check in the database is there any user associated with  this email
					if (!sqLiteHelper.isEmailExists(Email)) {

						//Email does not exist now add new user to database
						sqLiteHelper.addUser(new User(null, UserName, Email, Password));

						Toasty.success(RegisterActivity.this, "Contul a fost creat cu succes!", Toast.LENGTH_LONG, true).show();


						new Handler().postDelayed( new Runnable() {
							@Override
							public void run() {
								finish();
							}
						}, Toast.LENGTH_LONG);
					} else {

						Toasty.error(RegisterActivity.this, "Există deja un cont cu acest Email!.", Toast.LENGTH_LONG, true).show();
					}


				}
			}
		});
	}

	//this method used to set Login TextView click event


	//this method is used to connect XML views to its Objects
	private void initViews() {
		editTextEmail = findViewById(R.id.editText_Email);
		editTextPassword = findViewById(R.id.editText_password);
		editTextName = findViewById(R.id.editText_Nume);
		buttonRegister = findViewById(R.id.button_register);

	}

	//This method is used to validate input given by user
	public boolean validate() {

		boolean valid = false;

		//Get values from EditText fields
		String UserName = editTextName.getText().toString();
		String Email = editTextEmail.getText().toString();
		String Password = editTextPassword.getText().toString();

		//Handling validation for UserName field
		if (UserName.isEmpty()) {
			valid = false;
			editTextName.setError("Introduce un nume valid!");
		} else {
			if (UserName.length() > 5) {
				valid = true;
				editTextName.setError(null);
			} else {
				valid = false;
				editTextName.setError("Numele este prea scurt!");
			}
		}

		//Handling validation for Email field
		if (!android.util.Patterns.EMAIL_ADDRESS.matcher(Email).matches()) {
			valid = false;
			editTextEmail.setError("Introduce un email valid!");
		} else {
			valid = true;
			editTextEmail.setError(null);
		}

		//Handling validation for Password field
		if (Password.isEmpty()) {
			valid = false;
			editTextPassword.setError("Introduce o parola valida!");
		} else {
			if (Password.length() > 5) {
				valid = true;
				editTextPassword.setError(null);
			} else {
				valid = false;
				editTextPassword.setError("Parola este prea scurta!");
			}
		}


		return valid;

	}


	public void backToLogin( View view )
	{
		startActivity( new Intent(RegisterActivity.this,LoginActivity.class) );
	}
}