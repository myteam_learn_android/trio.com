package com.usebastian.trio;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import es.dmoral.toasty.Toasty;

public class LoginActivity extends AppCompatActivity
{

	private EditText editTextEmail, editTextPassword;
	private Button buttonLogin;
	private SQLiteHelper sqLiteHelper;

	//This method is for handling fromHtml method deprecation
	@SuppressWarnings( "deprecation" )
	public static Spanned fromHtml( String html )
	{
		Spanned result;
		if( android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N )
		{
			result = Html.fromHtml( html, Html.FROM_HTML_MODE_LEGACY );
		}
		else
		{
			result = Html.fromHtml( html );
		}
		return result;
	}

	@Override
	protected void onCreate( Bundle savedInstanceState )
	{
		super.onCreate( savedInstanceState );
		setContentView( R.layout.activity_login );

		sqLiteHelper = new SQLiteHelper( this );
		initViews();

		//set click event of login button
		buttonLogin.setOnClickListener( new View.OnClickListener()
		{
			@Override
			public void onClick( View view )
			{

				//Check user input is correct or not
				if( validate() )
				{

					//Get values from EditText fields
					String Email = editTextEmail.getText().toString();
					String Password = editTextPassword.getText().toString();

					//Authenticate user
					User currentUser = sqLiteHelper.Authenticate( new User( null, null, Email, Password ) );

					//Check Authentication is successful or not
					if( currentUser != null )
					{
						Toasty.success( LoginActivity.this, "Logat cu succes!", Toast.LENGTH_LONG, true ).show();

						//User Logged in Successfully Launch You home screen activity
               /* Intent intent=new Intent(LoginActivity.this,HomeScreenActivity.class);
                startActivity(intent);
                finish();*/

						Intent login = new Intent( LoginActivity.this, MainActivity.class );
						startActivity( login );
					}
					else
					{

						//User Logged in Failed
						Toasty.error( LoginActivity.this, "Eroare! Nu exista niciun cont asociat cu acest Email", Toast.LENGTH_LONG, true ).show();
					}
				}
			}
		} );
	}

	private boolean validate()
	{

		boolean valid;

		//Get values from EditText fields
		String Email = editTextEmail.getText().toString();
		String Password = editTextPassword.getText().toString();

		//Handling validation for Email field
		if( !android.util.Patterns.EMAIL_ADDRESS.matcher( Email ).matches() )
		{
			valid = false;
			editTextEmail.setError( "Introduce un email valid!" );
		}
		else
		{
			valid = true;
			editTextEmail.setError( null );
		}

		//Handling validation for Password field
		if( Password.isEmpty() )
		{
			valid = false;
			editTextPassword.setError( "Introduce o parola valida!" );
		}
		else
		{
			if( Password.length() > 5 )
			{
				valid = true;
				editTextPassword.setError( null );
			}
			else
			{
				valid = false;
				editTextPassword.setError( "Parola este prea scurta, < 5 !" );
			}
		}

		return valid;
	}

	//this method is used to connect XML views to its Objects
	private void initViews()
	{
		editTextEmail = findViewById( R.id.editText_Email );
		editTextPassword = findViewById( R.id.editText_password );
		buttonLogin = findViewById( R.id.button_login );
	}

	public void toRegister( View view )
	{
		startActivity( new Intent( LoginActivity.this, RegisterActivity.class ) );
	}
}